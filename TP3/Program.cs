﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MovieCollection.WaltDisneyMovies> moviesList = new MovieCollection().Movies;
            // Count all movies.
            DisplayQuestion("Count all movies.");
            Console.WriteLine(moviesList.Count);

            // Count all movies with the letter e.
            DisplayQuestion("Count all movies with the letter e.");
            Console.WriteLine(moviesList.Count(movie => movie.Title.Contains('e')));

            // Count how many time the letter f is in all the titles from this list.
            DisplayQuestion("Count how many time the letter f is in all the titles from this list.");
            Console.WriteLine(moviesList.Sum(movie => movie.Title.Count(c => c == 'f')));

            // Display the title of the film with the higher budget.
            DisplayQuestion("Display the title of the film with the higher budget.");
            Console.WriteLine(moviesList.Aggregate((m1, m2) => m1.Budget > m2.Budget ? m1 : m2).Title);

            // Display the title of the movie with the lowest box office.
            DisplayQuestion("Display the title of the movie with the lowest box office.");
            Console.WriteLine(moviesList.Aggregate((m1, m2) => m1.BoxOffice < m2.BoxOffice ? m1 : m2).Title);

            // Order the movies by reversed alphabetical order and print the first 11 of the list.
            DisplayQuestion("Order the movies by reversed alphabetical order and print the first 11 of the list.");
            foreach (var waltDisneyMovies in
                (from movie in moviesList orderby movie.Title descending select movie).Take(11))
            {
                Console.WriteLine(waltDisneyMovies.Title);
            }

            // Count all the movies made before 1980.
            DisplayQuestion("Count all the movies made before 1980.");
            Console.WriteLine(moviesList.Count(movie => movie.ReleaseDate.Year < 1980));

            // Count and display the average of all box offices every 10 years (1940’s, 1950’s, 1960’s... up to 2010’s).
            DisplayQuestion(
                "Count and display the average of all box offices every 10 years (1940’s, 1950’s, 1960’s... up to 2010’s).");
            foreach (var d in from movie in moviesList
                group movie by movie.ReleaseDate.Year - (movie.ReleaseDate.Year % 10)
                into g
                where g.Key <= 2010
                select new {Year = g.Key, Average = g.Average(a => a.BoxOffice)})
            {
                Console.WriteLine("{0}'s = {1}", d.Year, d.Average);
            }

            // Display the average running time of movies having a vowel as the first letter.
            DisplayQuestion("Display the average running time of movies having a vowel as the first letter.");
            Console.WriteLine((from movie in moviesList
                where "aeiou".IndexOf(movie.Title.ToLower()[0]) >= 0
                select movie.RunningTime).Average());

            // Calculate the mean of all Budget / Box Office of every movie ever
            DisplayQuestion("Calculate the mean of all Budget / Box Office of every movie ever");
            Console.WriteLine("Average Budget {0}, Average Box Office {1}", moviesList.Average(movie => movie.Budget),
                moviesList.Average(movie => movie.BoxOffice));

            // Print all movies with the letter H or W in the title, but not the letter I or T.
            DisplayQuestion("Print all movies with the letter H or W in the title, but not the letter I or T.");
            foreach (var waltDisneyMovies in from movie in moviesList
                where (movie.Title.ToUpper().Contains('H') || movie.Title.ToUpper().Contains('W')) &&
                      !(movie.Title.ToUpper().Contains('I') || movie.Title.ToUpper().Contains('T'))
                select movie)
            {
                Console.WriteLine(waltDisneyMovies.Title);
            }

            DisplayQuestion("Question 2" + Environment.NewLine + "# Create a simple function that create 3 threads");
            CreateThread();
        }

        public static void DisplayQuestion(string question)
        {
            Console.WriteLine("#--------------------------------------------------------------------#");
            Console.Write("# ");
            Console.WriteLine(question);
            Console.WriteLine("#--------------------------------------------------------------------#");
        }

        // Exercise 2: 
        // Create a simple function that create 3 threads:
        public static void CreateThread()
        {
            Thread t1 = new Thread(new ThreadStart(Thread1));
            Thread t2 = new Thread(new ThreadStart(Thread2));
            Thread t3 = new Thread(new ThreadStart(Thread3));
            t1.Start();
            t2.Start();
            t3.Start();

            t1.Join();
            t2.Join();
            t3.Join();
            Console.WriteLine("End of threads");
        }

        public static void Thread1()
        {
            var startTime = DateTime.UtcNow;
            while (DateTime.UtcNow - startTime < TimeSpan.FromSeconds(10))
            {
                print('_');
                Thread.Sleep(50);
            }
        }

        public static void Thread2()
        {
            var startTime = DateTime.UtcNow;
            while (DateTime.UtcNow - startTime < TimeSpan.FromSeconds(11))
            {
                print('*');
                Thread.Sleep(40);
            }
        }

        public static void Thread3()
        {
            var startTime = DateTime.UtcNow;
            while (DateTime.UtcNow - startTime < TimeSpan.FromSeconds(9))
            {
                print('°');
                Thread.Sleep(20);
            }
        }


        private static readonly Mutex m = new Mutex();

        public static void print(char c)
        {
            m.WaitOne();
            try
            {
                Console.WriteLine(c);
            }
            finally
            {
                m.ReleaseMutex();
            }
        }
    }
}